package Lesson_28;

public class Cars {
        public String model;
        public int yearOfManufacture;
        public double weight;
        public double length;
        public int maxSpeed;
        public int fuelConsuption;
        private int numberOfHumans = 100;

    public Cars(String model, int yearOfManufacture, double weight, double length, int maxSpeed, int fuelConsuption){
            this.model = model;
            this.yearOfManufacture = yearOfManufacture;
            this.weight = weight;
            this.length = length;
            this.maxSpeed = maxSpeed;
            this.fuelConsuption = fuelConsuption;
        }

        public int getNumberOfHumans(){
            return this.numberOfHumans;
        }
        public void setNumberOfHumans(int numberOfHumans){
            this.numberOfHumans = numberOfHumans;
        }

        double miles;
        public void calculateMiles (int klm){
            miles =  klm * 0.621371;
        }
        public double getMiles(){
            return miles;
        }

        public void km(){
        int km = 100;
        if (km == 100){
            System.out.println("У тебя средняя скорость");
        }
        else if (km == 120){
            System.out.println("Ты уже гонишь");
        }
        else if (km == 140){
            System.out.println("Превышен лимит");
        }
    }
    }
