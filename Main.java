package Lesson_28;

public class Main {
    public static void main(String[] args) {
        Cars cars = new Cars("Cars", 2000, 1000, 1000, 120, 10);
        Mercedes Smart = new Mercedes("Smart", 2004, 730, 2500, 135, 5);
        Toyota Camry = new Toyota("Camry", 2001, 1500, 4760, 220, 16);

        // Назначение количества мест в случае использования private int numberOfHumans.
        Smart.setNumberOfHumans(2);
        System.out.println(Smart.getNumberOfHumans());
        Camry.setNumberOfHumans(5);
        System.out.println(Camry.getNumberOfHumans());

        // Комменты при различных скоростях. cars = 100 км/ч, Smart = 120 км/ч и Camry = 140 км/ч.
        cars.km();
        Smart.km();
        Camry.km();

        // Перевод км и мили.
        cars.calculateMiles(120);
        System.out.println(cars.getMiles());

        // Остановка, когда бензин = 0.
        int benz;
        for (benz = 33; benz >= 0; benz--) {
            if (benz == 0 ) {
                System.out.println("Стоим. Бензин = " + benz);
                System.out.println("Ахтунг! Бензин капут!");
            }
        }
    }
}
