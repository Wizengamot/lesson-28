package Lesson_28;

import java.sql.SQLSyntaxErrorException;

public class Mercedes extends Cars {
    int volumeOfTank = 33;

    public Mercedes(String model, int yearOfManufacture, double weight, double length, int maxSpeed, int fuelConsuption) {
        super(model, yearOfManufacture, weight, length, maxSpeed, fuelConsuption);
    }

    public void km() {
        int km = 120;
        if (km == 100) {
            System.out.println("Не надрывайся");
        } else if (km == 120) {
            System.out.println("Почти предел");
        } else if (km == 140) {
            System.out.println("Не обольщайся");
        }
    }
}
