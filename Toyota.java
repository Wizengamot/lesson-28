package Lesson_28;

public class Toyota extends Cars {
    int volumeOfTank = 50;

    public Toyota(String model, int yearOfManufacture, double weight, double length, int maxSpeed, int fuelConsuption) {
        super(model, yearOfManufacture, weight, length, maxSpeed, fuelConsuption);
    }

    public void km(){
        int km = 140;
        if (km == 100){
            System.out.println("Слишком легко");
        }
        else if (km == 120){
            System.out.println("Среднячок");
        }
        else if (km == 140){
            System.out.println("Ты можешь больше");
        }
    }


}
